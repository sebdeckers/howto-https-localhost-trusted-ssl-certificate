# Instructions for macOS

This should work with Safari, Firefox, Chrome, and any application that is sufficiently integrated with the native macOS crypto APIs.

This certificate is added as a Root Certificate and only works with `localhost` domain. You should not hand it out to anyone, nor can you use this technique to sign certificates that other people can use to access your website.

Use a free certificate from [Letsencrypt](https://letsencrypt.org) instead for public domains.

## Step 1. Generate a key and a certificate.

Open a terminal and run:

```
openssl req -newkey rsa:2048 -new -nodes -sha256 -x509 -days 365 -keyout key.pem -out cert.pem -subj '/CN=localhost'
```

Sample output:
```
Generating a 2048 bit RSA private key
.............+++
.....+++
writing new private key to 'key.pem'
-----
```

This generates two files:

- `key.pem` is the private key your server uses to encrypt data. Never publish this. 
- `cert.pem` is the public certificate your server sends to the browser. This is what needs to be trusted by the client.

## Step 2. Add the certificate to Keychain

There are three ways to do this.

### Method A. Terminal

From the terminal, `cd` to the directory where you generated the certificate file, and run:

```
open cert.pem
```

Confirm by pressing *Add* (the default keychain should be fine).

![Add key confirmation dialog](screenshots/add_certificate_confirmation.png)

### Method B. Finder

Use *Finder* to locate the `cert.pem` and double click it. This launches Keychain to confirm.

![Folder containing certificate file](screenshots/finder_files.png)

Confirm by pressing *Add* (the default keychain should be fine).

![Add key confirmation dialog](screenshots/add_certificate_confirmation.png)


### Method C. Keychain

Open Spotlight (*⌘+Space*) to launch *Keychain Access*.

![Spotlight search results for Keychain Access](screenshots/spotlight_keychain.png)

Go to *File > Import Items* ...

![File > Import Items](screenshots/file_menu_import_items.png)

... and choose the `cert.pem` file.

![Open file dialog](screenshots/open_file_dialog.png)

## Step 3. Trust the Certificate

Double click the `localhost` entry in Keychain, or right click and choose *Get Info*.

![Right click > Get Info](screenshots/right_click_get_info.png)

This shows the certificate's details.

![Certificate information](screenshots/certificate_info.png)

Expand the *When using this certificate* menu and choose *Always trust*.

![Trust level menu selection](screenshots/trust_selection.png)

## Done!

Your browsers will now trust any connection to a server on `https://localhost/` (any port). Might need to close and reopen any already running tabs (or entire browsers).

![Chrome Security Overview is okay](screenshots/chrome_security_overview.png)
